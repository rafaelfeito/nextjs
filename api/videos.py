
from sanic import Sanic
from sanic.response import json

import urllib.request
import urllib.parse
import re
from fast_youtube_search import search_youtube
app = Sanic()

@app.route('/')
@app.route('/<path:path>')
async def index(request, path=""):
    print (request.args)
    q=request.args["query"]
    results = search_youtube(q, max_num_results=10)
    for x in results:
        x.update({"video_url":"https://www.youtube.com/watch?v="+x["id"]})
    print(results)
    return json(results)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)